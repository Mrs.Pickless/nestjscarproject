import { BadRequestException } from "@nestjs/common"
import { Test } from "@nestjs/testing"
import { AuthService } from "./auth.service"
import { User } from "./user.entity"
import { UsersService } from "./users.service"

describe('AuthService', () => {
    let service: AuthService
    let fakeUserService: Partial<UsersService>

    beforeEach(async () => {
        //create a fake copy of user service
        const users :User[]= []
        fakeUserService = {
            find: (email:String) => {
                const filteredUsers = users.filter(user => user.email === email)
                return Promise.resolve(filteredUsers)
            },
            create: (email, password) =>{
                const user = ({id:Math.floor(Math.random() * 999999), email :email, password :password} as User)
                users.push(user)
                return Promise.resolve(user)
        }
        }
        const module = await Test.createTestingModule({
            providers: [AuthService, { provide: UsersService, useValue: fakeUserService }]
        }).compile()
        service = module.get(AuthService)
    })

    it('can create an instance of AuthService', async () => {
        expect(service).toBeDefined();
    })

    it('can create a new user with hashed an salt password', async () => {
        const user = await service.signup("asf@gmail.com", "blabla")

        expect(user.password).not.toEqual("blabla")
        const [salt, hash] = user.password.split('.')
        expect(salt).toBeDefined()
        expect(hash).toBeDefined()
    })

    it("signup should return an error cause email already used", async () => {
        await service.signup("asf@gmail.com", "blabla")
        await expect(service.signup("asf@gmail.com", "blabla")).rejects.toThrowError()
    })

    it('throws if signin is called with a incorrect email', async ()=>{
        await expect(service.signin("asf@gmail.com", "blabla")).rejects.toThrowError()
    })
    it('throws if signin is called with invalid password', async ()=>{
        await service.signup("asf@gmail.com", "blabla")
        await expect(service.signin("asf@gmail.com", "b")).rejects.toThrowError()
    })

    it("should return User is correct password provided", async ()=>{
        await service.signup("asf@gmail.com", "blabla")
        const user = await service.signin("asf@gmail.com", "blabla")
        expect(user).toBeDefined()
    })
})